---
title: "Il mio primo programma in Arduino"
date: 2019-10-01T15:48:34+02:00
draft: false
---

![ image](/107.jpg)



Ciao voglio iniziare un breve viaggio nel mondo della elettronica, senza le complicazioni della troppa matematica ma con il desiderio di capire davvero cosa c'è sotto il luccicante mondo di smartphone, droni e pc.
Se infatti tempo fa questi oggetti mi apparivano come incomprensibili capolavori di tecnologia dopo avere appreso qualche facile nozione fondamentale di elettronica e elettricità mi appaiono adesso meno incomprensibili.
Ti confesso che invece di aspettare il nuovo table o Iphone adesso mi è aumentata la voglia di costruire qualcosa di mio, magari non all' altezza dei prodotti industriali maa qualcosa che possa dire di avere progettato e costruito da solo.

Il mio primo problema e stato capire come "infilare" dentro un robot o un drone tutta la tecnologia necessaria a controllarlo: la risposta e' stata immediata e ti basterà cercare in rete per trovare la risposta:
1) Arduino o 2) Raspberry.
Cosa sono? Li puoi considerare dei computer in miniatura, soprattutto il secondo, ma ancora una volta nessun miracolo tecnologicp: alla fine sono solo dei telefonini senza schermo e batterie!



```

int mioPin = 13; // LED connesso alla porta 13

void setup()
{
pinMode(mioPin, OUTPUT); // imposta il pin come “uscita” di un segnale digitale
}

void loop() // esegue questa sequenza all’infinito
{
digitalWrite(mioPin, HIGH); // accendi il LED
delay(1000); // aspetta un secondo
digitalWrite(mioPin, LOW); // spegni il LED
delay(1000); // aspetta un seconso
}

```



Senza Arduino e Raspberry non potrai fare molto nel campo della  tecnologia e della automazione, dovresti progettare da zero i tuoi "ferri del mestiere" e io ti sconsiglio vivamente di provarci!

All interno di Arduino trovi un microprocessore molto semplificato ma potente abbastanza da compiere una enormità di compiti davvero interessanti:
- Pilotare da remoto un drone, controllare temperature e umidità nella produzione di cibi e bevande (birra ad esempio), connettersi alla rete wifi e interfacciarsi a decine di sensori diversi, programmare la logica di controllo di apparecchiature industriali, usare i mille dati raccolti dai sensori per fare delle scelte ricavate dal software che ci caricheremo dentro.

Io mi interesso soprattutto di Arduino perchè è davvero uno strumento potente e super economico e anche in caso di rottura completa al massimo perdo pochi euro. Il costo non è un elemento secondario: pensa se dovessi fare esperimenti come se faceva 10 anni fa con le schede da inserire nel Pc: un errore e addio Pc!

Arduino è versatile, possiede un suo linguaggio di programmazione ispirato al C++ e se non sai programmare ti consiglio di imparare al più presto!

Ma non deprimerti, programmare Arduino è molto più semplice di quello che credi e inoltre puoi usare con minime modifiche le migliaia di programmi già disponibile in rete!

Il resto dei programmi proverò a svilupparlo assieme a te passo per passo e se avrai dei dubbi puoi sempre commentare in fondo ai post: rispondo sempre in meno di 24 ore!

Per completare il quadro della situazione Arduino e Raspberry sono dei capolavori del digitale ma non servirebbero a nulla senza dei componenti accessori che avrai visto milioni di volte senza farci troppo caso, sono le resistenza e i condensatori.

Ho perso un sacco di tempo nella mia breve avventura con Arduino trascurando resistenze e condensatori, e sai perchè? Perchè non avevo la conoscenza minima per trattare con cose tipo corrente e tensione e miei  progetti e 90 su 100 funzionavano male o non funzionavano per nulla!

Ma non ti preoccupare, quasi tutto quello che vedrai nel blog è stato testato dal sottoscritto partendo da progetti originali o da progetti molto "standard" diffusi in rete: in questo modo riuscir evitartele e resterà solo il divertimento.

Perchè resistenze e condensatori possono farti perdere un mare di tempo? perchè sono dispositivi "analogici"
che lavorano non un sacco di tensioni e corrente diverse e non sempre la loro precisione è massima:
Sommando approssimazioni e errori di tensione in un circuito appena in poco compicato i mal di testa sono assicurati.

E se vuoi andare al sodo la prima cosa da capire è la differenza (per nulla evidente) tra tensione e corrente, due concetti facilissimi da capire ma che sono stati resi un verso enigma dalle mille spiegazioni errate che qualcuno avrà sentito alle superiori.
Capire la tensione e la corrente non è più difficile che capire questo esempio:
La pressione a cui esce l'acqua da un tubo (tensione) e i litri al secondo che escono (corrente) . FINE!
E se ti stupisci del perchè tante persone non sanno spiegare concetti così banali la spiegazione è semplice: non li hanno mai capiti neppure loro!

Ma anche se la differenza non ti è ancora chiara ricorda che nel mondo magico del digitale spesso si si deve occupare solo della tensione e questo onestamente ha reso popolare il compute tra gli hobbisti!

E se ti piace l' elettronica in genere ma non hai mai pensato di costruire qualcosa di tuo, voglio concludere questo post ricordando un fatto di politica estera avvento dell' Agosto 2019:
premesso che questo blog non si occupa di politica ma solo di tecnica,  in quel periodo dei droni costruiti "artigianalmente" e senza supporto industriale nello Yemen hanno compiuto una missione militare complicatissima colpendo, senza fare vittime un impianto petrolifero a 1200 km di distanza.

Si è trattato di un punto di svolta "epocale" nella politica, ma non solo: si è capito che grazie alla inventiva anche dei semplici appassionati posso imitare (anzi superare) i prodotti costruiti industrialmente.

Anche tu puoi progettare e costruire il tuo drone/robot/controllo automatico di grande <strong>successo</strong> e dare una svolta alla tua vita o almeno al tuo hobby!

Le regole di base della tecnologia sono spesso semplici e se avrai la pazienza di leggere il mio blog a poco a poco capirai che il mistero che avvolge il mondo dell' elettronica è piu un "mindset", una atteggiamento mentale che una realtà.
